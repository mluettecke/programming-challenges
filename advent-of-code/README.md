# Advent of Code


<img align="right" width="150" height="150" src="christmas-tree.png">


[Advent of Code]() is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like.

## 2018

| Day |                                           | Part I  | Part II | Finished         |
|-----|-------------------------------------------|---------|---------|------------------|
| 1   | [Chronal Calibration](2018/day01)         | :star2: | :star2: | :christmas_tree: |
| 2   | [Inventory Management System](2018/day02) | :star2: | :star2: | :christmas_tree: |
| 3   | [No Matter How You Slice It](2018/day03)  | :star2: | :star2: | :christmas_tree: |
| 4   | [Repose Record](2018/day04)               | :star2: | :star2: | :christmas_tree: |
| 5   | [Alchemical Reduction](2018/day05)        | :star2: | :star2: | :christmas_tree: |
| 6   | Chronal Coordinates                       |         |         |                  |
| 7   | The Sum of Its Parts                      |         |         |                  |
| 8   | Memory Maneuver                           |         |         |                  |
| 9   | Marble Mania                              |         |         |                  |
| 10  | The Stars Align                           |         |         |                  |
| 11  | Chronal Charge                            |         |         |                  |
| 12  | Subterranean Sustainability               |         |         |                  |
| 13  | Mine Cart Madness                         |         |         |                  |
| 14  | Chocolate Charts                          |         |         |                  |
| 15  | Beverage Bandits                          |         |         |                  |
| 16  | Chronal Classification                    |         |         |                  |
| 17  | Settlers of The North Pole                |         |         |                  |
| 18  |                                           |         |         |                  |
| 19  |                                           |         |         |                  |
| 20  |                                           |         |         |                  |
| 21  |                                           |         |         |                  |
| 22  |                                           |         |         |                  |
| 23  |                                           |         |         |                  |
| 24  |                                           |         |         |                  |
---
<div>Icon made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
