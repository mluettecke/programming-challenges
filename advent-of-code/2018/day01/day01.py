import itertools
import pathlib


def main():
    input_file = open('input', 'r')
    sum = 0
    for line in input_file.readlines():
        sum += int(line)
    print("Solution for part one: {}".format(sum))
    sum = 0
    seen_frequencies = set()
    flag = True
    while flag:
        input_file.seek(0)

        for line in input_file.readlines():
            sum += int(line)
            if sum in seen_frequencies:
                print("Solution for part two: {}".format(sum))
                flag = False
                break
            else:
                seen_frequencies.add(sum)


if __name__ == '__main__':
    main()
