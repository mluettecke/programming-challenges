''' Advent of Code 2018
    Day 2: Inventory Management System
    https://adventofcode.com/2018/day/2
'''
import os
from collections import Counter


def part_one():
    '''
    '''
    input_file = open(os.path.join(os.path.dirname(__file__), 'input'), 'r')
    twice = 0
    thrice = 0
    for id in input_file.readlines():
        char_count = Counter(id)

        if 2 in char_count.values():
            twice += 1
        if 3 in char_count.values():
            thrice += 1

    return twice * thrice


def part_two():
    input_file = open(os.path.join(os.path.dirname(__file__), 'input'), 'r')

    ids = []
    for id in input_file.readlines():
        for known_id in ids:
            id_difference = sum(1 for a, b in zip(id, known_id) if a != b)
            if id_difference == 1:
                return ''.join(a for a, b in zip(id, known_id) if a == b)
        ids.append(id)


def main():
    print("Solution for part one: {}".format(part_one()))
    print("Solution for part two: {}".format(part_two()))


if __name__ == '__main__':
    main()
