''' Advent of Code 2018
    Day 5: Alchemical Reduction
    https://adventofcode.com/2018/day/5
'''
import os
import string
from string import ascii_lowercase, ascii_uppercase


def react_polymer(polymer):
    reacted_polymer = ['']
    for char in polymer:
        if char == reacted_polymer[-1].swapcase():
            reacted_polymer.pop()
        else:
            reacted_polymer.append(char)
    return ''.join(reacted_polymer)


def part_one(polymer):
    """ How many units remain after fully reacting the polymer you scanned? 
    """
    return len(react_polymer(polymer))


def part_two(polymer):
    """ What is the length of the shortest polymer you can produce by removing 
        all units of exactly one type and fully reacting the result?
    """
    # temp variable to watch shortest length achieved
    min_len = len(polymer)
    for letter in string.ascii_lowercase:
        # remove upper- and lowercase occurences of current letter
        temp_poly = polymer.replace(letter, '').replace(letter.upper(), '')
        temp_poly = react_polymer(temp_poly)
        min_len = min(len(temp_poly), min_len)
    return min_len


def main():
    with open(os.path.join(
            os.path.dirname(__file__), 'input'), 'r') as f:
        polymer = f.read().strip()

    print("Solution for part one: {}".format(part_one(polymer)))
    print("Solution for part two: {}".format(part_two(polymer)))


if __name__ == '__main__':
    main()
