import os
from operator import itemgetter
from collections import defaultdict


def manhatten_distance(point_1, point_2):
    """ Calculate the manhattan distance between two points
    """
    return abs(point_1[0] - point_2[0]) + abs(point_1[1] - point_2[1])


def solve(coordinates):
    """ What is the size of the largest area that isn't infinite?
    """
    x_min = min(coordinates, key=lambda x: x[0])[0]
    x_max = max(coordinates, key=lambda x: x[0])[0]
    y_min = min(coordinates, key=lambda x: x[1])[1]
    y_max = max(coordinates, key=lambda x: x[1])[1]

    # iterate through all possible points
    areas = defaultdict(int)
    infinity = set()
    for x in range(x_min, x_max + 1):
        for y in range(y_min, y_max + 1):
            point = (x, y)
            # calculate distance between point and all coordinates
            distances = []
            for coordinate in coordinates:
                distances.append(
                    (manhatten_distance(point, coordinate), coordinate))
            distances.sort()
            if distances[0][0] != distances[1][0]:
                # the first entry has the smalles distance now
                areas[distances[0][1]] += 1
                # fuckin long if condition here
                # checks if it is infinite by checkin if a x or y is 0 or
                # infinite via x_max and y_max
                if point[0] == x_max or point[1] == y_max or point[0] == 0 or point[1] == 0:
                    infinity.add(distances[0][1])

    # throw out all infinite areas
    for area in infinity:
        areas.pop(area)
    print("Solution for part one: {}".format(max(areas.values())))

    target_region_count = 0
    for x in range(x_min, x_max + 1):
        for y in range(y_min, y_max + 1):
            point = (x, y)
            sum_distances = 0
            for coordinate in coordinates:
                sum_distances += manhatten_distance(point, coordinate)
            # if the sum of all distances is under 10000 then 
            # the point is located inside the the target region
            if sum_distances < 10000:
                target_region_count += 1
    print("Solution for part two: {}".format(target_region_count))     


def main():
    input_file = open(os.path.join(
        os.path.dirname(__file__), 'input'), 'r')

    # convert the input coordinates to a list of int tuples
    coordinates = []
    for line in input_file:
        coord = line.split(',')
        coordinates.append((int(coord[0]), int(coord[1])))

    solve(coordinates)

if __name__ == '__main__':
    main()
