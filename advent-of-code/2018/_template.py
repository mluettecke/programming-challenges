''' Advent of Code 2018
    Day XX:
    https://adventofcode.com/2018/day/X
'''


def part_one():
    """ 
    """
    pass


def part_two():
    """ 
    """
    pass


def main():
    print("Solution for part one: {}".format(part_one()))
    print("Solution for part two: {}".format(part_two()))


if __name__ == '__main__':
    main()
