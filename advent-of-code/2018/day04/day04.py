''' Advent of Code 2018
    Day 4: Repose Record
    https://adventofcode.com/2018/day/4
'''
import os
import re
from collections import defaultdict
from datetime import datetime, timedelta


def solve(sleep_records):
    """
    """
    # parse records and accumulate guard minutes
    # 1. Regex parse
    # 2. Add minutes to respective guard via start - end time
    guards = {}
    asleep = False
    current_guard = None
    sleep_start = None
    for entry in sleep_records:
        match = re.match(r'\[(.*)\] (.*)', entry)
        if not match:
            continue
        time = datetime.strptime(match.group(1), '%Y-%m-%d %H:%M')
        if 'falls asleep' in match.group(2):
            sleep_start = time.minute
            continue
        if 'wakes up' in match.group(2):
            for i in range(sleep_start, time.minute):
                # for every minute in range from falling asleep to waking up increment that minute
                guards[current_guard_id][i] += 1
                continue
        m = re.search(r'\#(\d*)', match.group(2))
        if m:
            current_guard_id = int(m.group(1))
            if not current_guard_id in guards:
                guards[current_guard_id] = defaultdict(int)
            continue

    guard_most_sleep = 0
    for guard, sleep in guards.items():
        sleep_sum = sum(sleep.values())
        if sleep_sum > guard_most_sleep:
            guard_most_sleep = guard
    # Part One solution
    print("Solution for part one: {}".format(guard_most_sleep *
                                             max(guards[guard_most_sleep], key=guards[guard_most_sleep].get)))

    # Great short name :D
    guards_most_sleep_by_minute = [0 for x in range(60)]
    for minute in range(0, 60):
        for guard, sleep in guards.items():
            if guards_most_sleep_by_minute[minute] < sleep[minute]:
                guards_most_sleep_by_minute[minute] = guard
    print(guards_most_sleep_by_minute)


def main():
    input_file = open(os.path.join(
        os.path.dirname(__file__), 'input'), 'r')
    sleep_records = sorted(input_file.read().splitlines())

    solve(sleep_records)


if __name__ == '__main__':
    main()
