''' Advent of Code 2018
    Day 3: No Matter How You Slice It
    https://adventofcode.com/2018/day/3
'''
import os
import re

from collections import defaultdict


def parse_claim(claim_string):
    """ Parse the claim string and return its value.
        Solved with a simple regex
    """
    id, x, y, width, height = map(int, re.findall(r'\d+', claim_string))
    return id, x, y, width, height


def get_coords(x, y, width, height):
    """ Returns the claimed coordinates(inches) for
        the given claim data
    """
    # walk through square and return all coordinates
    for x_coord in range(width):
        for y_coord in range(height):
            yield (x + x_coord, y + y_coord)


def part_one():
    """ 
    """
    input_file = open(os.path.join(
        os.path.dirname(__file__), 'input'), 'r')
    claimed_inches_counts = defaultdict(int)
    for claim in input_file.readlines():
        _, x, y, width, height = parse_claim(claim)
        for coord in get_coords(x, y, width, height):
            claimed_inches_counts[coord] += 1
    overlap_count = 0
    for value in claimed_inches_counts.values():
        if value > 1:
            overlap_count += 1
    return overlap_count


def part_two():
    """
    """
    input_file = open(os.path.join(
        os.path.dirname(__file__), 'input'), 'r')
    claimed_inches_counts = defaultdict(int)
    # Set with all claims to later subtract the overlapping ones from
    all_claims = set()
    overlapped_claims = set()
    for claim in input_file.readlines():
        claim_id, x, y, width, height = parse_claim(claim)
        # all_claims.add(claim_id)
        for coord in get_coords(x, y, width, height):
            claimed_inches_counts[coord] += 1
            # if claimed_inches_counts[coord] > 1:
            #    overlapped_claims.add(claim_id)
    input_file.seek(0)
    for claim in input_file.readlines():
        claim_id, x, y, width, height = parse_claim(claim)
        all_claims.add(claim_id)
        for coord in get_coords(x, y, width, height):
            if claimed_inches_counts[coord] > 1:
                overlapped_claims.add(claim_id)
    return (all_claims - overlapped_claims)


def main():
    print("Solution for part one: {}".format(part_one()))
    print("Solution for part two: {}".format(part_two()))


if __name__ == '__main__':
    main()
