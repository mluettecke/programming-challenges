# Programming-Practice 
<p align="left">

<img src="https://img.shields.io/github/last-commit/mluettecke/programming-challenges.svg"/>
<img src="https://img.shields.io/github/license/mluettecke/programming-challenges.svg"/>
<img src="https://img.shields.io/github/languages/count/mluettecke/programming-challenges.svg"/>
</p>

My solutions to various programming-challenges, coding-katas, programming-problems and similar stuff :

**Programming Problems / Katas**
  * [Project Euler](#project-euler) (profile)

**Programming Challenges**
  * [Advent of Code](#advent-of-code-2018)

## Programming Problems / Katas
### Project Euler

| ID      | Description            | Rust                    | C   | Cpp | Python | Java                    |
| ------- | ---------------------- | ----------------------- | --- | --- | ------ | ----------------------- |
| **001** | Multiples of 3 and 5   | :heavy_check_mark: Done | -   | -   | -      | :heavy_check_mark: Done |
| **002** | Even Fibonacci numbers | -                       | -   | -   | -      | -                       |
| **003** | Largest prime factor   | -                       | -   | -   | -      | -                       |

## Programming Challenges
### Advent of Code

#### 2018
